# vuejs

Ejercicios y proyectos con Vue.js

# Introducción básica

- ["Vue.js - Introducción"](https://soka.gitlab.io/blog/post/2019-03-18-introduccion-vue/).
- ["Vue.js - Introducción: Condicionales v-if y v-show"](https://soka.gitlab.io/blog/post/2019-03-27-introduccion-vue-2/).
- ["Vue.js - Introducción: Bucles v-for"]().

# Manuales turoriales información

![](https://d33wubrfki0l68.cloudfront.net/565916198b0be51bf88b36f94b80c7ea67cafe7c/7f70b/cover.png)

- ["Advanced R - Hadley Wickham"](https://adv-r.hadley.nz/).

# Herramientas

- [Vue devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=es): Extensión para navegador para inspeccionar DOM Vue.js.
- [Visual Studio Code](https://code.visualstudio.com/): IDE Entorno edición código.

# Librerias

- [Vuesax](https://lusaxweb.github.io/vuesax/): Framework para frontend componentes Vue.
- [Gridsome](https://gridsome.org/): Permite crear Webs estáticas y apps con Vue.js.
